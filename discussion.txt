GIT VCS

What is Git VCS?
- Git VCS is an open source version control version system. It allows is to track and update our files with only the changes made to it.

Gitlab vs Github
    There is not much diffrence between the two. Both Gitlab and Github are just competing cloud services that store and manage our online repositories.

Two Types Repositories
    - Local Repository
      Folders that use git technology. therefore, it allows us to track changes made in the files with the folder. These changes can then be uploaded and update the files in our remote Repositories.
    - Remote Repository
      These are the folders that use git technology but instead located in the internet or in a clod services such as Gitlab or Github.

What is an SSH key?
- SSH or "Secure Shell Key" are tools we use to authenticate the uploading or of other tasks when manipulating or using git repositories. it allow us to push/upload changes to our repos without the need of password.

Git Commands:
   -git status
   To peek at the states of the files/folders in the repository.
   -git init
   To initilize a local Git repository.
   -git add .
   To track all the changes that we.ve made and prepare these files as a new version to be uploaded.
   -git commit -m "<commitMessage>"
   To create a new commit or version repo:
   -git log
   To check the version history of the repository.
   -git remote add <aliasOf Remote> <urlOfRemote>
   To add/connect a remote repository to ou local repository
   -git remote -v
   To view the remote repositories connected to our local repo:
   -git push <alias> master
   To push our updates/changes/version/commit into our remote repository.




Git config
-These commands will allow us to have git recognize the person trying to push into na online/remote repo:

git config --global user.email "emailUsedInGitLab"
-Configure the email used to push into the remote repo.

git config --global user.name "userNameUsedInGitLab"
-Configure the name/user of the user trying to push into gitlab

git config --global --list
-Checks whether the user infornation has been successfully added.


